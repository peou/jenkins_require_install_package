# Travellist - Laravel Demo App

This is a Laravel demo application.

Relevant Tutorials:

- [How to Install and Configure Laravel with LEMP on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-laravel-with-lemp-on-ubuntu-18-04)


<!-- Docker Reference -->
<!-- 
    https://www.digitalocean.com/community/tutorials/how-to-install-and-set-up-laravel-with-docker-compose-on-ubuntu-22-04
    https://dev.to/snakepy/my-favorite-laravel-development-environment-with-docker-nginx-php-fpm-xdebug-in-vscode-2o03
    https://jump24.co.uk/journal/turbocharged-php-development-with-xdebug-docker-and-phpstorm/
    https://blog.logrocket.com/how-to-run-laravel-docker-compose-ubuntu-v22-04/
    
 -->