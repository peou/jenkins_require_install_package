which bash

echo "=====OS Info====="
cat /etc/os-release
# Install sudo
apt-get update
apt install sudo -y
usermod -aG sudo root  #Assign sudo to group root

# Install nano
sudo apt install nano -y

# install yum4
sudo apt-get update && sudo apt-get -y install yum4

#install git
sudo apt update && sudo apt install git -y
git --version
