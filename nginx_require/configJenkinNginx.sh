#!/bin/bash

file="picstudio.vannsann.tech.conf"
domain="picstudio.vannsann.tech"
cd /etc/nginx/conf.d/
touch $file
echo 'server {
	listen 80;
	server_name $domain ;

        location / {
            proxy_set_header  Host $host;
            proxy_pass http://localhost:8100;
    }
}' >> $file

sudo nginx -t
systemctl reload nginx.service
sudo systemctl restart nginx.service
#sudo apt update && sudo apt install -y python3-certbot-nginx
certbot --nginx -d $domain
#sudo ufw allow 'Nginx HTTPS'
#sudo ufw status
#sudo ufw allow 'Nginx Full' 
