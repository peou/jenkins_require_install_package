#!/bin/bash

adduser sanny_john
usermod -aG sudo sanny_john
apt update
apt install ufw
ufw app list
ufw allow OpenSSH
ufw enable
ufw status
